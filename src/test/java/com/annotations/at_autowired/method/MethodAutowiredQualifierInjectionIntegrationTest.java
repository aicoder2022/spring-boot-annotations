package com.annotations.at_autowired.method;

import com.annotations.ArbitraryClassOrComponent;
import com.annotations.at_autowired.field.ApplicationContextTestAutowiredQualifier;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

// Annotations Tutorial:
//      https://www.baeldung.com/spring-annotations-resource-inject-autowire
// Covering the important annotations i.e. @Autowired, @Resource and @Inject (related ones and also with their nuances)
//  @Resource and @Inject belong to the javax package (javax.annotation.*)
//  @Autowired annotation belongs to the org.springframework.beans.factory.annotation package.
@SpringBootTest
@ContextConfiguration(
		loader=AnnotationConfigContextLoader.class,
		classes= ApplicationContextTestAutowiredQualifier.class
)
public class MethodAutowiredQualifierInjectionIntegrationTest {
	// 4. @Autowired
	// 4.2 Method Injection
	// 4.2.2 Match by Qualifier
	private ArbitraryClassOrComponent matchByQualifierDifferentFieldName;

	@Autowired
	@Qualifier("matchByQualifier")
	// passing any other value to the 'name' attribute will lead to NoSuchBeanDefinitionException exception
	public void setMatchByQualifier(ArbitraryClassOrComponent matchByQualifier) {
		this.matchByQualifierDifferentFieldName = matchByQualifier;
	}

	private ArbitraryClassOrComponent anotherMatchByQualifierDifferentFieldName;

	@Autowired
	@Qualifier("anotherMatchByQualifier")
	// passing any other value to the 'name' attribute will lead to NoSuchBeanDefinitionException exception
	public void setAnotherMatchByQualifier(ArbitraryClassOrComponent anotherMatchByQualifier) {
		this.anotherMatchByQualifierDifferentFieldName = anotherMatchByQualifier;
	}

	private ArbitraryClassOrComponent someMatchByQualifierDifferentFieldName;

	@Autowired
	@Qualifier("disparateMatchByQualifier")
	// passing any other value to the 'name' attribute will lead to NoSuchBeanDefinitionException exception
	// the qualifier name (as also defined in the Configuration) and the local field names CAN BE different
	// as long as the qualifier name can be found and the field types are the same
	public void setDisparateMatchByQualifier(ArbitraryClassOrComponent disparateMatchByQualifier) {
		this.someMatchByQualifierDifferentFieldName = disparateMatchByQualifier;
	}

	@Test
	public void givenAutowiredAnnotationByQualifier_WhenOnField_ThenDependencyValid() {
		assertNotNull(matchByQualifierDifferentFieldName);
		assertEquals("matchByQualifier", matchByQualifierDifferentFieldName.getValue());
	}

	@Test
	public void givenAutowiredAnnotationByAnotherQualifier_WhenOnField_ThenDependencyValid() {
		assertNotNull(anotherMatchByQualifierDifferentFieldName);
		assertEquals("anotherMatchByQualifier", anotherMatchByQualifierDifferentFieldName.getValue());
	}

	@Test
	public void givenAutowiredAnnotationByDifferentQualifierAndFieldNames_WhenOnField_ThenDependencyValid() {
		assertNotNull(someMatchByQualifierDifferentFieldName);
		assertEquals("disparateMatchByQualifier", someMatchByQualifierDifferentFieldName.getValue());
	}

}
