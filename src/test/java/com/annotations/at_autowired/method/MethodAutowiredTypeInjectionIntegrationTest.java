package com.annotations.at_autowired.method;

import com.annotations.ArbitraryClassOrComponent;
import com.annotations.at_autowired.field.ApplicationContextTestAutowiredType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

// Annotations Tutorial:
//      https://www.baeldung.com/spring-annotations-resource-inject-autowire
// Covering the important annotations i.e. @Autowired, @Resource and @Inject (related ones and also with their nuances)
//  @Resource and @Inject belong to the javax package (javax.annotation.*)
//  @Autowired annotation belongs to the org.springframework.beans.factory.annotation package.
@SpringBootTest
@ContextConfiguration(
		loader=AnnotationConfigContextLoader.class,
		classes=ApplicationContextTestAutowiredType.class
)
public class MethodAutowiredTypeInjectionIntegrationTest {

	// 4. @Autowired
	// 4.2 Method Injection
	// 4.2.1 Match by Type

	private ArbitraryClassOrComponent matchByType;

	@Autowired
	public void setMatchByType(ArbitraryClassOrComponent matchByTypeInjectHere) {
		this.matchByType = matchByTypeInjectHere;
	}

	@Test
	public void givenAutowiredAnnotationByType_WhenOnField_ThenDependencyValid() {
		assertNotNull(matchByType);
		assertEquals("matchByType", matchByType.getValue());
	}

}
