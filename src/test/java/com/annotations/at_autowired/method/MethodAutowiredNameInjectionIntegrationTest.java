package com.annotations.at_autowired.method;

import com.annotations.ArbitraryClassOrComponent;
import com.annotations.at_autowired.field.ApplicationContextTestAutowiredName;
import com.annotations.at_autowired.ComponentWithValue;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

// Annotations Tutorial:
//      https://www.baeldung.com/spring-annotations-resource-inject-autowire
// Covering the important annotations i.e. @Autowired, @Resource and @Inject (related ones and also with their nuances)
//  @Resource and @Inject belong to the javax package (javax.annotation.*)
//  @Autowired annotation belongs to the org.springframework.beans.factory.annotation package.

@SpringBootTest
@ContextConfiguration(
		loader= AnnotationConfigContextLoader.class,
		classes=ApplicationContextTestAutowiredName.class
)
public class MethodAutowiredNameInjectionIntegrationTest {

	// 4. @Autowired
	// 4.2 Method Injection
	// 4.2.3 Match by Name

	private ArbitraryClassOrComponent matchByName;

	@Autowired
	public void setMatchByName(ArbitraryClassOrComponent matchByName) {
		this.matchByName = matchByName;
	}

	private ComponentWithValue matchByComponentValue;

	@Autowired
	public void setMatchByComponentValue(ComponentWithValue matchByComponentValue) {
		this.matchByComponentValue = matchByComponentValue;
	}

	@Test
	public void givenAutowiredAnnotationByName_WhenOnField_ThenDependencyValid() {
		assertNotNull(matchByName);
		assertEquals("matchByName", matchByName.getValue());
	}

	@Test
	public void givenAutowiredAnnotationByComponentValue_WhenOnField_ThenDependencyValid() {
		assertNotNull(matchByComponentValue);
		assertEquals("byComponentValue", matchByComponentValue.getValue());
	}
}
