package com.annotations.at_resource.method;

import com.annotations.at_resource.ApplicationContextTestResourceName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import javax.annotation.Resource;
import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

// Annotations Tutorial:
//      https://www.baeldung.com/spring-annotations-resource-inject-autowire
// Covering the important annotations i.e. @Autowired, @Resource and @Inject (related ones and also with their nuances)
//  @Resource and @Inject belong to the javax package (javax.annotation.*)
//  @Autowired annotation belongs to the org.springframework.beans.factory.annotation package.
@SpringBootTest
@ContextConfiguration(
		loader=AnnotationConfigContextLoader.class,
		classes=ApplicationContextTestResourceName.class
)
public class MethodResourceNameInjectionIntegrationTest {

	private File matchByName;

	// 2. @Resource
	// 2.2 Setter Injection
	// 2.2.1 Match by Name
	@Resource(name="matchByName")
	// passing any other value to the 'name' attribute will lead to NoSuchBeanDefinitionException exception
	protected void setMatchByName(File matchByName) {
		this.matchByName = matchByName;
	}

	@Test
	public void givenResourceAnnotationByName_WhenOnField_ThenDependencyValid() {
		assertNotNull(matchByName);
		assertEquals("matchByName.txt", matchByName.getName());
	}
}
