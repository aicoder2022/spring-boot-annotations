package com.annotations.at_resource.method;

import com.annotations.at_resource.ApplicationContextTestResourceType;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import javax.annotation.Resource;

import static com.annotations.at_resource.ApplicationContextTestResourceType.CustomType;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

// Annotations Tutorial:
//      https://www.baeldung.com/spring-annotations-resource-inject-autowire
// Covering the important annotations i.e. @Autowired, @Resource and @Inject (related ones and also with their nuances)
//  @Resource and @Inject belong to the javax package (javax.annotation.*)
//  @Autowired annotation belongs to the org.springframework.beans.factory.annotation package.
@SpringBootTest
@ContextConfiguration(
		loader=AnnotationConfigContextLoader.class,
		classes=ApplicationContextTestResourceType.class
)
public class MethodResourceTypeInjectionIntegrationTest {

	// 2. @Resource
	// 2.2 Setter Injection
	// 2.2.2 Match by Type
	private CustomType matchByType;

	@Resource
	protected void setCustomType(CustomType matchByType) {
		this.matchByType = matchByType;
	}

	@Test
	public void givenResourceAnnotationByType_WhenOnField_ThenDependencyValid() {
		assertNotNull(matchByType);
		assertEquals("matchByType", matchByType.getValue());
	}
}
