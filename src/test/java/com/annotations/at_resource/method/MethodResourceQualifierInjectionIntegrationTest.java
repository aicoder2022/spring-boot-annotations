package com.annotations.at_resource.method;

import com.annotations.at_resource.ApplicationContextTestResourceQualifier;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Qualifier;
import com.annotations.at_resource.ApplicationContextTestResourceQualifier.CustomType;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

// Annotations Tutorial:
//      https://www.baeldung.com/spring-annotations-resource-inject-autowire
// Covering the important annotations i.e. @Autowired, @Resource and @Inject (related ones and also with their nuances)
//  @Resource and @Inject belong to the javax package (javax.annotation.*)
//  @Autowired annotation belongs to the org.springframework.beans.factory.annotation package.
@SpringBootTest
@ContextConfiguration(
		loader=AnnotationConfigContextLoader.class,
		classes=ApplicationContextTestResourceQualifier.class
)
public class MethodResourceQualifierInjectionIntegrationTest {
	private CustomType matchByQualifier;

	// 2. @Resource
	// 2.2 Setter Injection
	// 2.2.3 Match by Qualifier
	@Resource
	@Qualifier("matchByQualifier")
	// passing any other value to the 'name' attribute will lead to NoSuchBeanDefinitionException exception
	public void setMatchByQualifier(CustomType matchByQualifier) {
		this.matchByQualifier = matchByQualifier;
	}

	private CustomType anotherMatchByQualifier;

	@Resource
	@Qualifier("anotherMatchByQualifier")
	// passing any other value to the 'name' attribute will lead to NoSuchBeanDefinitionException exception
	public void setAnotherMatchByQualifier(CustomType anotherMatchByQualifier) {
		this.anotherMatchByQualifier = anotherMatchByQualifier;
	}

	private CustomType someMatchByQualifier;

	@Resource
	@Qualifier("disparateMatchByQualifier")
	// passing any other value to the 'name' attribute will lead to NoSuchBeanDefinitionException exception
	// the qualifier name (as also defined in the Configuration) and the local field names CAN BE different
	// as long as the qualifier name can be found and the field types are the same
	public void setSomeMatchByQualifier(CustomType someMatchByQualifier) {
		this.someMatchByQualifier = someMatchByQualifier;
	}

	@Test
	public void givenResourceAnnotationByQualifier_WhenOnField_ThenDependencyValid() {
		assertNotNull(matchByQualifier);
		assertEquals("matchByQualifier", matchByQualifier.getValue());
	}

	@Test
	public void givenResourceAnnotationByAnotherQualifier_WhenOnField_ThenDependencyValid() {
		assertNotNull(anotherMatchByQualifier);
		assertEquals("anotherMatchByQualifier", anotherMatchByQualifier.getValue());
	}

	@Test
	public void givenResourceAnnotationByDifferentQualifierAndFieldNames_WhenOnField_ThenDependencyValid() {
		assertNotNull(someMatchByQualifier);
		assertEquals("disparateMatchByQualifier", someMatchByQualifier.getValue());
	}

}
