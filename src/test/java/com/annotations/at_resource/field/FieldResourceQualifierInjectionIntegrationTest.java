package com.annotations.at_resource.field;

import com.annotations.at_resource.ApplicationContextTestResourceQualifier;
import com.annotations.at_resource.ApplicationContextTestResourceQualifier.CustomType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

// Annotations Tutorial:
//      https://www.baeldung.com/spring-annotations-resource-inject-autowire
// Covering the important annotations i.e. @Autowired, @Resource and @Inject (related ones and also with their nuances)
//  @Resource and @Inject belong to the javax package (javax.annotation.*)
//  @Autowired annotation belongs to the org.springframework.beans.factory.annotation package.
@SpringBootTest
@ContextConfiguration(
		loader=AnnotationConfigContextLoader.class,
		classes=ApplicationContextTestResourceQualifier.class
)
public class FieldResourceQualifierInjectionIntegrationTest {
	// 2. @Resource
	// 2.1 Field Injection
	// 2.1.3 Match by Qualifier
	@Resource
	@Qualifier("matchByQualifier")
	// passing any other value to the 'name' attribute will lead to NoSuchBeanDefinitionException exception
	private CustomType matchByQualifier;

	@Resource
	@Qualifier("anotherMatchByQualifier")
	// passing any other value to the 'name' attribute will lead to NoSuchBeanDefinitionException exception
	private CustomType anotherMatchByQualifier;

	@Resource
	@Qualifier("disparateMatchByQualifier")
	// passing any other value to the 'name' attribute will lead to NoSuchBeanDefinitionException exception
	// the qualifier name (as also defined in the Configuration) and the local field names CAN BE different
	// as long as the qualifier name can be found and the field types are the same
	private CustomType someMatchByQualifier;

	@Test
	public void givenResourceAnnotationByQualifier_WhenOnField_ThenDependencyValid() {
		assertNotNull(matchByQualifier);
		assertEquals("matchByQualifier", matchByQualifier.getValue());
	}

	@Test
	public void givenResourceAnnotationByAnotherQualifier_WhenOnField_ThenDependencyValid() {
		assertNotNull(anotherMatchByQualifier);
		assertEquals("anotherMatchByQualifier", anotherMatchByQualifier.getValue());
	}

	@Test
	public void givenResourceAnnotationByDifferentQualifierAndFieldNames_WhenOnField_ThenDependencyValid() {
		assertNotNull(someMatchByQualifier);
		assertEquals("disparateMatchByQualifier", someMatchByQualifier.getValue());
	}

}
