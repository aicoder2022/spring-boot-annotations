package com.annotations.at_resource.field;

import com.annotations.at_resource.ApplicationContextTestResourceType;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import javax.annotation.Resource;

import static com.annotations.at_resource.ApplicationContextTestResourceType.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

// Annotations Tutorial:
//      https://www.baeldung.com/spring-annotations-resource-inject-autowire
// Covering the important annotations i.e. @Autowired, @Resource and @Inject (related ones and also with their nuances)
//  @Resource and @Inject belong to the javax package (javax.annotation.*)
//  @Autowired annotation belongs to the org.springframework.beans.factory.annotation package.
@SpringBootTest
@ContextConfiguration(
		loader=AnnotationConfigContextLoader.class,
		classes=ApplicationContextTestResourceType.class
)
public class FieldResourceTypeInjectionIntegrationTest {

	// 2. @Resource
	// 2.1 Field Injection
	// 2.1.2 Match by Type
	@Resource
	private CustomType matchByType;

	@Test
	public void givenResourceAnnotationByType_WhenOnField_ThenDependencyValid() {
		assertNotNull(matchByType);
		assertEquals("matchByType", matchByType.getValue());
	}
}
