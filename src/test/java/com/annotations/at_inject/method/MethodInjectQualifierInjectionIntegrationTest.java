package com.annotations.at_inject.method;

import com.annotations.at_inject.ApplicationContextTestInjectQualifier;
import com.annotations.ArbitraryClassOrComponent;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

// Annotations Tutorial:
//      https://www.baeldung.com/spring-annotations-resource-inject-autowire
// Covering the important annotations i.e. @Autowired, @Resource and @Inject (related ones and also with their nuances)
//  @Resource and @Inject belong to the javax package (javax.annotation.*)
//  @Autowired annotation belongs to the org.springframework.beans.factory.annotation package.
@SpringBootTest
@ContextConfiguration(
		loader=AnnotationConfigContextLoader.class,
		classes=ApplicationContextTestInjectQualifier.class
)
public class MethodInjectQualifierInjectionIntegrationTest {
	private ArbitraryClassOrComponent matchByQualifier;

	// 3. @Inject
	// 3.2 Setter Injection
	// 3.2.3 Match by Qualifier
	@Inject
	@Qualifier("matchByQualifier")
	// passing any other value to the 'name' attribute will lead to NoSuchBeanDefinitionException exception
	public void setMatchByQualifier(ArbitraryClassOrComponent matchByQualifier) {
		this.matchByQualifier = matchByQualifier;
	}

	private ArbitraryClassOrComponent anotherMatchByQualifier;

	@Inject
	@Qualifier("anotherMatchByQualifier")
	// passing any other value to the 'name' attribute will lead to NoSuchBeanDefinitionException exception
	public void setAnotherMatchByQualifier(ArbitraryClassOrComponent anotherMatchByQualifier) {
		this.anotherMatchByQualifier = anotherMatchByQualifier;
	}

	private ArbitraryClassOrComponent someMatchByQualifier;

	@Inject
	@Qualifier("disparateMatchByQualifier")
	// passing any other value to the 'name' attribute will lead to NoSuchBeanDefinitionException exception
	// the qualifier name (as also defined in the Configuration) and the local field names CAN BE different
	// as long as the qualifier name can be found and the field types are the same
	public void setSomeMatchByQualifier(ArbitraryClassOrComponent someMatchByQualifier) {
		this.someMatchByQualifier = someMatchByQualifier;
	}

	@Test
	public void givenResourceAnnotationByQualifier_WhenOnField_ThenDependencyValid() {
		assertNotNull(matchByQualifier);
		assertEquals("matchByQualifier", matchByQualifier.getValue());
	}

	@Test
	public void givenResourceAnnotationByAnotherQualifier_WhenOnField_ThenDependencyValid() {
		assertNotNull(anotherMatchByQualifier);
		assertEquals("anotherMatchByQualifier", anotherMatchByQualifier.getValue());
	}

	@Test
	public void givenResourceAnnotationByDifferentQualifierAndFieldNames_WhenOnField_ThenDependencyValid() {
		assertNotNull(someMatchByQualifier);
		assertEquals("disparateMatchByQualifier", someMatchByQualifier.getValue());
	}

}
