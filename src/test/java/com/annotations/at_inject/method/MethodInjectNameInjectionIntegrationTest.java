package com.annotations.at_inject.method;

import com.annotations.at_inject.ApplicationContextTestInjectName;
import com.annotations.ArbitraryClassOrComponent;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import javax.inject.Inject;
import javax.inject.Named;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

// Annotations Tutorial:
//      https://www.baeldung.com/spring-annotations-resource-inject-autowire
// Covering the important annotations i.e. @Autowired, @Resource and @Inject (related ones and also with their nuances)
//  @Resource and @Inject belong to the javax package (javax.annotation.*)
//  @Autowired annotation belongs to the org.springframework.beans.factory.annotation package.
@SpringBootTest
@ContextConfiguration(
		loader=AnnotationConfigContextLoader.class,
		classes=ApplicationContextTestInjectName.class
)
public class MethodInjectNameInjectionIntegrationTest {

	private ArbitraryClassOrComponent matchByName;

	// 3. @Inject
	// 3.2 Setter Injection
	// 3.2.3 Match by Name
	@Inject
	@Named("matchByName")
	// passing any other value to the 'name' attribute will lead to NoSuchBeanDefinitionException exception
	protected void setMatchByName(ArbitraryClassOrComponent matchByName) {
		this.matchByName = matchByName;
	}

	@Test
	public void givenResourceAnnotationByName_WhenOnField_ThenDependencyValid() {
		assertNotNull(matchByName);
		assertEquals("matchByName", matchByName.getValue());
	}
}
