package com.annotations.at_inject.field;

import com.annotations.at_inject.ApplicationContextTestInjectType;
import com.annotations.ArbitraryClassOrComponent;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

// Annotations Tutorial:
//      https://www.baeldung.com/spring-annotations-resource-inject-autowire
// Covering the important annotations i.e. @Autowired, @Resource and @Inject (related ones and also with their nuances)
//  @Resource and @Inject belong to the javax package (javax.annotation.*)
//  @Autowired annotation belongs to the org.springframework.beans.factory.annotation package.
@SpringBootTest
@ContextConfiguration(
		loader=AnnotationConfigContextLoader.class,
		classes=ApplicationContextTestInjectType.class
)
public class FieldInjectTypeInjectionIntegrationTest {

	// 3. @Inject
	// 3.1 Field Injection
	// 3.1.1 Match by Type
	@Inject
	private ArbitraryClassOrComponent matchByType;

	@Test
	public void givenInjectAnnotationByType_WhenOnField_ThenDependencyValid() {
		assertNotNull(matchByType);
		assertEquals("matchByType", matchByType.getValue());
	}

}
