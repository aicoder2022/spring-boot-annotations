package com.annotations.at_inject;

import com.annotations.ArbitraryClassOrComponent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

// Annotations Tutorial:
//      https://www.baeldung.com/spring-annotations-resource-inject-autowire
// Covering the important annotations i.e. @Autowired, @Resource and @Inject (related ones and also with their nuances)
//  @Resource and @Inject belong to the javax package (javax.annotation.*)
//  @Autowired annotation belongs to the org.springframework.beans.factory.annotation package.
@Configuration
public class ApplicationContextTestInjectName {
    // 3. @Resource
    // 3.1 Field Injection
    // 3.1.3 Match by Name

    @Bean
    public ArbitraryClassOrComponent matchByName() {
        return new YetAnotherArbitraryClassOrComponent();
    }
}