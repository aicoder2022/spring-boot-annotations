package com.annotations.at_inject;

import com.annotations.ArbitraryClassOrComponent;

public class YetAnotherArbitraryClassOrComponent extends ArbitraryClassOrComponent {
    private final String label = "matchByName";

    @Override
    public String getValue() {
        return label;
    }
}