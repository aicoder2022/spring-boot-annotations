package com.annotations.at_inject;

import com.annotations.ArbitraryClassOrComponent;

public class AnotherArbitraryClassOrComponent extends ArbitraryClassOrComponent {
    private final String label = "matchByQualifier";

    @Override
    public String getValue() {
        return label;
    }
}