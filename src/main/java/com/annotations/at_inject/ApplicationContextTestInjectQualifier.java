package com.annotations.at_inject;

import com.annotations.ArbitraryClassOrComponent;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

// Annotations Tutorial:
//      https://www.baeldung.com/spring-annotations-resource-inject-autowire
// Covering the important annotations i.e. @Autowired, @Resource and @Inject (related ones and also with their nuances)
//  @Resource and @Inject belong to the javax package (javax.annotation.*)
//  @Autowired annotation belongs to the org.springframework.beans.factory.annotation package.
@Configuration
public class ApplicationContextTestInjectQualifier {
    // 3. @Inject
    // 3.1 Field Injection
    // 3.1.3 Match by Qualifier
    @Bean
    @Qualifier("matchByQualifier")
    public ArbitraryClassOrComponent matchByQualifier(){
        return new ArbitraryClassOrComponent("matchByQualifier");
    }

    @Bean
    @Qualifier("anotherMatchByQualifier")
    public ArbitraryClassOrComponent anotherMatchByQualifier(){
        return new ArbitraryClassOrComponent("anotherMatchByQualifier");
    }

    @Bean
    @Qualifier("disparateMatchByQualifier")
    // the qualifier name (as also defined in the Configuration) and the local field names CAN BE different
    // as long as the qualifier name can be found and the field types are the same
    public ArbitraryClassOrComponent someMatchByQualifier(){
        return new ArbitraryClassOrComponent("disparateMatchByQualifier");
    }
}
