package com.annotations.at_autowired.field;

import com.annotations.ArbitraryClassOrComponent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages={"com.annotations"})
public class ApplicationContextTestAutowiredName {

    @Bean
    public ArbitraryClassOrComponent matchByName() {
        return new ArbitraryClassOrComponent("matchByName");
    }

}
