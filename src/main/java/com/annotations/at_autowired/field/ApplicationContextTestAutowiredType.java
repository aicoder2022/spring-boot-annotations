package com.annotations.at_autowired.field;

import com.annotations.ArbitraryClassOrComponent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationContextTestAutowiredType {

    @Bean
    public ArbitraryClassOrComponent autowiredFieldDependency() {
        return new ArbitraryClassOrComponent();
    }

}
