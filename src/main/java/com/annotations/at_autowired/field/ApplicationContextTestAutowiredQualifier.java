package com.annotations.at_autowired.field;

import com.annotations.ArbitraryClassOrComponent;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationContextTestAutowiredQualifier {

    @Bean
    @Qualifier("matchByQualifier")
    public ArbitraryClassOrComponent matchByQualifier() {
        return new ArbitraryClassOrComponent("matchByQualifier");
    }

    @Bean
    @Qualifier("anotherMatchByQualifier")
    public ArbitraryClassOrComponent anotherMatchByQualifier() {
        return new ArbitraryClassOrComponent("anotherMatchByQualifier");
    }

    @Bean
    @Qualifier("disparateMatchByQualifier")
    public ArbitraryClassOrComponent disparateMatchByQualifier() {
        return new ArbitraryClassOrComponent("disparateMatchByQualifier");
    }

}
