package com.annotations.at_autowired;
import org.springframework.stereotype.Component;

@Component(value="byComponentValue")
public class ComponentWithValue {
    private String label = "byComponentValue";

    public String getValue() {
        return label;
    }
}