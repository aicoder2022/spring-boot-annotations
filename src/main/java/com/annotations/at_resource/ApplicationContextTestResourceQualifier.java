package com.annotations.at_resource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;

// Annotations Tutorial:
//      https://www.baeldung.com/spring-annotations-resource-inject-autowire
// Covering the important annotations i.e. @Autowired, @Resource and @Inject (related ones and also with their nuances)
//  @Resource and @Inject belong to the javax package (javax.annotation.*)
//  @Autowired annotation belongs to the org.springframework.beans.factory.annotation package.
@Configuration
public class ApplicationContextTestResourceQualifier {
    // 2. @Resource
    // 2.1 Field Injection
    // 2.1.3 Match by Qualifier
    @Bean
    @Qualifier("matchByQualifier")
    public CustomType matchByQualifier(){
        return new CustomType("matchByQualifier");
    }

    @Bean
    @Qualifier("anotherMatchByQualifier")
    public CustomType anotherMatchByQualifier(){
        return new CustomType("anotherMatchByQualifier");
    }

    @Bean
    @Qualifier("disparateMatchByQualifier")
    // the qualifier name (as also defined in the Configuration) and the local field names CAN BE different
    // as long as the qualifier name can be found and the field types are the same
    public CustomType someMatchByQualifier(){
        return new CustomType("disparateMatchByQualifier");
    }

    public static class CustomType {
        private String value;

        public CustomType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

}
