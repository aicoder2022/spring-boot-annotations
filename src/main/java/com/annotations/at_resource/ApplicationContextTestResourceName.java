package com.annotations.at_resource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
// Annotations Tutorial:
//      https://www.baeldung.com/spring-annotations-resource-inject-autowire
// Covering the important annotations i.e. @Autowired, @Resource and @Inject (related ones and also with their nuances)
//  @Resource and @Inject belong to the javax package (javax.annotation.*)
//  @Autowired annotation belongs to the org.springframework.beans.factory.annotation package.
@Configuration
public class ApplicationContextTestResourceName {
    // 2. @Resource
    // 2.1 Field Injection
    // 2.1.1 Match by Name
    @Bean(name="matchByName")
    // passing any other value to the 'name' attribute will lead to NoSuchBeanDefinitionException exception
    public File matchByName(){
        return new File("matchByName.txt");
    }
}
