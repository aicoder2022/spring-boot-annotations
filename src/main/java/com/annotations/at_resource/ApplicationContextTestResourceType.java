package com.annotations.at_resource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

// Annotations Tutorial:
//      https://www.baeldung.com/spring-annotations-resource-inject-autowire
// Covering the important annotations i.e. @Autowired, @Resource and @Inject (related ones and also with their nuances)
//  @Resource and @Inject belong to the javax package (javax.annotation.*)
//  @Autowired annotation belongs to the org.springframework.beans.factory.annotation package.
@Configuration
public class ApplicationContextTestResourceType {
    // 2. @Resource
    // 2.1 Field Injection
    // 2.1.2 Match by Type
    @Bean
    public CustomType matchByType(){
        return new CustomType("matchByType");
    }

    public static class CustomType {
        private String value;

        public CustomType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

}
