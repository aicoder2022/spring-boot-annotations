package com.annotations;

import org.springframework.stereotype.Component;

@Component
public class ArbitraryClassOrComponent {
    private String label = "matchByType";

    public ArbitraryClassOrComponent() {}

    public ArbitraryClassOrComponent(String label) {
        this.label = label;
    }

    public String getValue() {
        return label;
    }
}